<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

use PHPUnit_Framework_TestCase as PHPUnitTestCase;
use Psr\Http\Message\ResponseInterface;
use Bastard\Response\JsonResponseType;
use Bastard\Response\XmlResponseType;
use Bastard\Response\ResponseBuffer;
use Slim\Http\Response;

class ResponseTypeTestCase extends PHPUnitTestCase
{
    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return new Response();
    }

    /**
     * @return ResponseBuffer
     */
    public function getResponseBuffer(): ResponseBuffer
    {
        return new ResponseBuffer($this->getResponse());
    }

    /**
     * @return JsonResponseType
     */
    public function getJsonResponse(): JsonResponseType
    {
        return new JsonResponseType($this->getResponseBuffer());
    }

    /**
     * @return XmlResponseType
     */
    public function getXmlResponse(): XmlResponseType
    {
        return new XmlResponseType($this->getResponseBuffer());
    }

    public function testJsonResponseType()
    {
        $r = $this->getJsonResponse();
        $r->setAllData([
            'first' => [
                'a key' => 'a value'
            ],
            'second' => [
                'multiple',
                'values'
            ]
        ]);

        $r = $r->respond($this->getResponse());

        $this->assertInstanceOf('\Psr\Http\Message\ResponseInterface', $r);
        $this->assertEquals(200, $r->getStatusCode());

        print_r($r);
    }
}