<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

use Slim\App;
use App\Controller\HomeController;

return function (App $app): App {
    $app->map(['POST', 'GET'], '/', HomeController::class)
        ->setName('home');

    $app->get('/welcome', HomeController::class . ':welcome')
        ->setName('welcome');

    $app->get('/show/{name}', HomeController::class . ':show')
        ->setName('show-name');

    return $app;
};