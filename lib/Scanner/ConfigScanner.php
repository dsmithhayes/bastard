<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Scanner;

use Bastard\Scanner;
use Extended\Collections\Queue\BasicQueue;

/**
 * Class ConfigScanner
 * @package Bastard
 */
class ConfigScanner extends Scanner
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ConfigReader constructor.
     * @param string $configPath
     */
    public function __construct(string $configPath)
    {
        while ($file = self::scanDir($configPath)) {
            $tmp = require "{$configPath}/{$file}";
            $this->config = array_merge($this->config, $tmp);
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        return $this->config[$name];
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->config;
    }
}