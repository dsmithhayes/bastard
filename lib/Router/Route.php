<?php

/**
 * @author Dave Smith-Hayes
 */

namespace Bastard\Router;

use Slim\App;

/**
 * Class Route
 * @package Bastard\Router
 */
abstract class Route
{
    /**
     * @param App $app
     * @return App
     */
    public function __invoke(App $app)
    {
        $this->invoke($app);
        return $app;
    }

    /**
     * @param App $app
     * @return App
     */
    abstract function invoke(App $app): App;
}