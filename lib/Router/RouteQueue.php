<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Router;

use Extended\Collections\Queue;

/**
 * Class RouteQueue
 * @package Bastard\Router
 */
class RouteQueue implements Queue
{
    /**
     * @var array
     */
    private $routes = [];

    /**
     * RouteQueue constructor.
     * @param array $routes
     */
    public function __construct(array $routes = [])
    {
        foreach ($routes as $route) {
            $this->enqueue($route);
        }
    }

    /**
     * @param mixed $route
     * @return $this
     */
    public function enqueue($route)
    {
        if (is_callable($route)) {
            array_push($this->routes, $route);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function dequeue()
    {
        return array_shift($this->routes);
    }
}