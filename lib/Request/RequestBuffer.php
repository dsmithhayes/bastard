<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Request;

use Bastard\Response\ResponseBuffer;
use Extended\Stream\Buffer;
use Psr\Http\Message\ServerRequestInterface as RequestInterface;

/**
 * Class RequestBuffer
 * @package Bastard\RequestInterface
 */
class RequestBuffer extends Buffer
{
    /**
     * RequestBuffer constructor.
     * @param null $buffer
     */
    public function __construct($buffer = null)
    {
        parent::__construct();

        if ($buffer instanceof RequestInterface) {
            $this->setCookies($buffer)
                 ->setGet($buffer)
                 ->setPost($buffer)
                 ->setBody($buffer);
        } else {
            $this->buffer = [
                'post' => [],
                'get' => [],
                'cookies' => [],
                'body' => '',
            ];
        }
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function setPost(RequestInterface $request)
    {
        $this->buffer['post'] = $request->getAttributes();
        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function setGet(RequestInterface $request)
    {
        $this->buffer['get'] = $request->getQueryParams();
        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function setCookies(RequestInterface $request)
    {
        $this->buffer['cookies'] = $request->getCookieParams();
        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function setBody(RequestInterface $request)
    {
        $this->buffer['body'] = $request->getBody();
        return $this;
    }
}