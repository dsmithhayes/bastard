<?php

namespace Bastard;

use Psr\Http\Message\ServerRequestInterface as RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class Middleware
{
    /**
     * @param RequestInterface $req
     * @param ResponseInterface $res
     * @param callable $next
     * @return ResponseInterface
     */
    public function __invoke(RequestInterface   $req,
                             ResponseInterface  $res,
                             callable           $next): ResponseInterface
    {
        return $this->invoke($req, $res, $next);
    }

    /**
     * @param RequestInterface $req
     * @param ResponseInterface $res
     * @param callable $next
     * @return ResponseInterface
     */
    abstract public function invoke(RequestInterface    $req,
                                    ResponseInterface   $res,
                                    callable            $next): ResponseInterface;
}