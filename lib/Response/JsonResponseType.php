<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Response;

use Bastard\Response\ResponseType;
use Psr\Http\Message\ResponseInterface;

/**
 * Class JsonResponseType
 * @package Bastard\ResponseInterface
 */
class JsonResponseType extends ResponseType
{
    /**
     * JsonResponseType constructor.
     * @param ResponseBuffer $buffer
     */
    public function __construct(ResponseBuffer $buffer)
    {
        $this->mimeType = 'application/json';
        parent::__construct($buffer);
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function respond(ResponseInterface $response): ResponseInterface
    {
        return $this->buffer->setHeader('Content-Type', $this->getMimeType())
                            ->setBody(json_encode($this->data))
                            ->prepareReponse($response);
    }
}