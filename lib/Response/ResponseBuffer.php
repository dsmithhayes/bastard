<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Response;

use Extended\Stream\Buffer;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseBuffer
 * @package Bastard\ResponseInterface
 */
class ResponseBuffer extends Buffer
{
    /**
     * @var string
     */
    const KEY_HEADERS = 'headers';

    /**
     * @var string
     */
    const KEY_BODY = 'body';

    /**
     * @var string
     */
    const KEY_STATUS_CODE = 'status_code';

    /**
     * @var array
     */
    protected $bufferKeys = [
        self::KEY_STATUS_CODE,
        self::KEY_HEADERS,
        self::KEY_BODY
    ];

    /**
     * The buffer is actually an array. It had the keys `'headers'`, `'body'`,
     * and `'status_code'`. You can pass an instance of the `ResponseInterface`
     * to be parsed, an array with the appropriate keys, or nothing at all.
     *
     * ResponseBuffer constructor.
     * @param ResponseInterface|array|null $buffer
     */
    public function __construct($buffer = null)
    {
        if ($buffer && $buffer instanceof ResponseInterface) {
            $response = [
                self::KEY_HEADERS => $buffer->getHeaders(),
                self::KEY_BODY => $buffer->getBody(),
                self::KEY_STATUS_CODE => $buffer->getStatusCode(),
            ];
        } else {
            $response = [
                self::KEY_HEADERS  => isset($buffer[self::KEY_HEADERS]) ?? [],
                self::KEY_BODY => isset($buffer[self::KEY_BODY]) ?? '',
                self::KEY_STATUS_CODE => isset($buffer[self::KEY_STATUS_CODE]) ?? 200,
            ];
        }

        parent::__construct($response);
    }

    /**
     * If an array is not passed as the input, the input is casted as a string
     * and set as the body.
     *
     * @param mixed $buffer
     * @return $this
     */
    public function input($buffer)
    {
        if ($buffer instanceof ResponseInterface) {
            return $this->parseResponseInterface($buffer);
        } elseif (is_array($buffer)) {
            return $this->parseArray($buffer);
        }

        $this->buffer[self::KEY_BODY] = (string) $buffer;
        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @return $this
     */
    protected function parseResponseInterface(ResponseInterface $response)
    {
        $this->buffer = [
            self::KEY_STATUS_CODE => $response->getStatusCode(),
            self::KEY_HEADERS => $response->getHeaders(),
            self::KEY_BODY => $response->getBody()
        ];

        return $this;
    }

    /**
     * @param array $response
     * @return $this
     */
    protected function parseArray(array $response)
    {
        foreach ($this->bufferKeys as $bufferKey) {
            if (array_key_exists($bufferKey, $response)) {
                $this->buffer[$bufferKey] = $response[$bufferKey];
            }
        }

        return $this;
    }

    /**
     * @param string $key
     * @param string $val
     * @return $this
     */
    public function setHeader(string $key, string $val)
    {
        $this->buffer[self::KEY_HEADERS][$key] = $val;
        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getHeader(string $key): string
    {
        return $this->buffer[self::KEY_HEADERS][$key];
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->buffer[self::KEY_HEADERS];
    }

    /**
     * @param int $code
     * @return $this
     */
    public function setStatusCode(int $code)
    {
        $this->buffer[self::KEY_STATUS_CODE] = $code;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->buffer[self::KEY_STATUS_CODE];
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setBody($value)
    {
        $this->buffer[self::KEY_BODY] = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->buffer[self::KEY_BODY];
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function prepareReponse(ResponseInterface $response): ResponseInterface
    {
        foreach ($this->getHeaders() as $name => $value) {
            $response->withHeader($name, $value);
        }

        $response->getBody()->write($this->getBody());
        return $response->withStatus($this->getStatusCode());
    }
}