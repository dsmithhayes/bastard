<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Response;

use Psr\Http\Message\ResponseInterface;
use Bastard\Response\ResponseBuffer;

/**
 * Class ResponseInterface
 * @package Bastard
 */
abstract class ResponseType
{
    /**
     * @var ResponseBuffer
     */
    protected $buffer;

    /**
     * This is intermediary data that will be used to form the body of the
     * buffer. It can be completely disregarded in favour of just setting the
     * buffer data directly.
     *
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    protected $mimeType = 'text/plain';

    /**
     * ResponseType constructor.
     * @param ResponseBuffer $buffer
     */
    public function __construct(ResponseBuffer $buffer)
    {
        $this->buffer = $buffer;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function setData(string $key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setAllData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    abstract public function respond(ResponseInterface $response): ResponseInterface;
}