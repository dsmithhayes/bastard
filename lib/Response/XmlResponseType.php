<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard\Response;

use Bastard\Response\ResponseType;
use Psr\Http\Message\ResponseInterface;
use DOMNode;
use DOMDocument;
use DOMElement;
use DOMAttr;
use DOMText;
use InvalidArgumentException;

/**
 * Class XmlResponseType
 * @package Bastard\Response
 */
class XmlResponseType extends ResponseType
{
    /**
     * XmlResponseType constructor.
     * @param ResponseBuffer $buffer
     */
    public function __construct(ResponseBuffer $buffer)
    {
        $this->mimeType = 'application/xml';
        $this->data['document'] = new DOMDocument('1.0', 'utf-8');
        parent::__construct($buffer);
    }

    /**
     * @return DOMDocument
     */
    public function getDocument()
    {
        return $this->data['document'];
    }

    /**
     * @param string $xml
     * @return bool
     */
    public function readFromString(string $xml): bool
    {
        return $this->getDocument()->loadXML($xml);
    }

    /**
     * @param string $filePath
     * @param int $options
     * @return bool
     */
    public function readFromFile(string $filePath, int $options = 0): bool
    {
        return $this->getDocument()->load($filePath, $options);
    }

    /**
     * @param string $name
     * @param string|null $value
     * @return DOMElement
     */
    public function elementFactory(string $name, string $value = ''): DOMElement
    {
        return $this->getDocument()->createElement($name, $value);
    }

    /**
     * @param DOMElement $element
     * @return $this
     */
    public function attachElement(DOMElement $element)
    {
        $this->getDocument()->appendChild($element);
        return $this;
    }

    /**
     * @param string $name
     * @return DOMAttr
     */
    public function attributeFactory(string $name, $value = null): DOMAttr
    {
        $attr = $this->getDocument()->createAttribute($name);
        $attr->value = (string) $value;
        return $attr;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setData(string $key, $value)
    {
        $element = $this->elementFactory($key);

        if ($value instanceof DOMNode) {
            $element->appendChild($value);
        } elseif (is_array($value)) {
            $this->setAllData($value);
        } else {
            // I don't care what your data actually is, its a string after this
            // point in code execution. - DSH
            $element->appendChild(new DOMText((string) $value));
        }

        $this->getDocument()->appendChild($element);
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setAllData(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $key = "n{$key}";
            }

            $this->setData($key, $value);
        }

        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function respond(ResponseInterface $response): ResponseInterface
    {
        return $this->buffer->setHeader('Content-Type', $this->getMimeType())
                            ->setBody($this->getDocument()->saveXML())
                            ->prepareReponse($response);
    }
}