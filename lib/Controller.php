<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard;

use Bastard\Response\ResponseBuffer;
use Bastard\Response\ResponseType;
use Bastard\Request\RequestBuffer;
use Psr\Http\Message\ServerRequestInterface as RequestInterface;
use Psr\Http\Message\ResponseInterface as ResponseInterface;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Controller
 * @package Bastard
 */
abstract class Controller
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var RequestBuffer
     */
    protected $requestBuffer;

    /**
     * @var ResponseBuffer
     */
    protected $responseBuffer;

    /**
     * @var ResponseType
     */
    protected $response;

    /**
     * Controller constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->requestBuffer = new RequestBuffer($container->request);

        /**
         * Basic plaintext response type.
         */
        $this->response = new class ($container->response) extends ResponseType
        {
            public function respond(): ResponseInterface {
                $buffer = (string) $this->getBody();
                $this->response->getBody()->write($buffer);
                return $this->response
                            ->withHeader('Content-Type', 'text/plain');
            }
        };

        $this->responseBuffer = $this->response->getBuffer();
    }

    /**
     * @param RequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return mixed
     */
    public function __invoke(RequestInterface $req, ResponseInterface $res, array $args = [])
    {
        return $this->index($req, $res, $args);
    }

    /**
     * @param RequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    abstract public function index(
        RequestInterface $req,
        ResponseInterface $res,
        array $args = []
    ): ResponseInterface;

    /**
     * Handles sending the data as JSON in a response. This can be overwritten
     * to handle other media types.
     *
     * @return ResponseInterface
     */
    public function render(): ResponseInterface
    {
        return $this->response->respond();
    }

    /**
     * @return RequestBuffer
     */
    public function getRequestBuffer(): RequestBuffer
    {
        return clone $this->requestBuffer;
    }

    /**
     * @return mixed
     */
    public function getRequestBody()
    {
        return $this->requestBuffer->output();
    }

    /**
     * @return ResponseBuffer
     */
    public function getResponseBuffer(): ResponseBuffer
    {
        return clone $this->responseBuffer;
    }

    /**
     * Ideally this is where one would tell the Controller which type of
     * response to send. By default all responses are in plaintext, and
     * Bastard provides a JSON and XML response type.
     *
     * @param ResponseType $response
     * @return $this
     */
    public function setResponseType(ResponseType $response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function set(string $key, $value)
    {
        $this->responseBuffer->input([$key => $value]);
        return $this;
    }
}