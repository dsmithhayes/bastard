<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard;

use Generator;

/**
 * Class Scanner
 * @package Bastard
 */
abstract class Scanner
{
    /**
     * @param string $path
     * @return Generator
     */
    public static function scanDir(string $path): Generator
    {
        foreach (scandir($path) as $file) {
            if ($file !== '.' && $file !== '..') {
                yield $file;
            }
        }
    }
}