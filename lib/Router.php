<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Bastard;

use Slim\App;
use Bastard\Router\RouteQueue;

/**
 * Class Router
 * @package Bastard
 */
class Router
{
    /**
     * @var RouteQueue
     */
    protected $routeQueue;

    /**
     * Hard set on having all of the routes mid-fixed with `.routes` to show
     * its a collection of routes.
     *
     * @var string
     */
    private $matchPattern = '/*\.routes\.php';

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routeQueue = new RouteQueue();
    }

    /**
     * The `$routePath` is a full path string of a file that it can open.
     *
     * @param string $routePath
     * @return $this
     */
    public function register(string $routePath)
    {
        foreach (scandir($routePath) as $file) {
            if ($file !== '.' && $file !== '..') {
                if (preg_match($this->matchPattern, $file)) {
                    $fileBuffer = require "{$routePath}/{$file}";
                    $this->routeQueue->enqueue($fileBuffer);
                }
            }
        }

        return $this;
    }

    /**
     * @param App $app
     * @return App
     */
    public function run(App $app): App
    {
        while ($route = $this->routeQueue->dequeue()) {
            $app = $route($app);
        }

        return $app;
    }

    /**
     * @return RouteQueue
     */
    public function getRouteQueue(): RouteQueue
    {
        return $this->routeQueue;
    }
}