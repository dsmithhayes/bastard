<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace App\Routes;

use App\Controller\HomeController;
use Bastard\Router\Route;
use Slim\App;

/**
 * Class HomeRoute
 * @package App\Routes
 */
class HomeRoute extends Route
{
    /**
     * @param App $app
     * @return App
     */
    public function invoke(App $app): App
    {
        $app->map(['POST', 'GET'], '/', HomeController::class)
            ->setName('home');

        $app->get('/welcome', HomeController::class . ':welcome')
            ->setName('welcome');

        $app->get('/show/{name}', HomeController::class . ':show')
            ->setName('show-name');

        return $app;
    }
}