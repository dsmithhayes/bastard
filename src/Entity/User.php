<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace App\Entity;

use Cake\ORM\Entity;

/**
 * Class User
 * @package App\Entity
 */
class User extends Entity
{
    /**
     * @var bool
     */
    private $useEmailAsUsername = false;
}