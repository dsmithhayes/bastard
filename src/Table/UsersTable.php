<?php

namespace App\Table;

use App\Entity\User;
use Cake\ORM\Table;

/**
 * Class UsersTable
 * @package App\Table
 */
class UsersTable extends Table
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        $this->setEntityClass(User::class);
    }
}