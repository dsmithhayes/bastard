<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace App\Controller;

use Bastard\Controller;
use Bastard\Response\JsonResponseType;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface as Container;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends Controller
{
    /**
     * HomeController constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $jsonResponseType = new JsonResponseType($container->response);
        $this->setResponseType($jsonResponseType);
        parent::__construct($container);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     */
    public function index(Request $req, Response $res, array $args = []): Response
    {
        return $this->welcome($req, $res, $args);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     */
    public function welcome(Request $req, Response $res, array $args = []): Response
    {
        $message = "Hello, and welcome to Bastard.";
        $this->responseBuffer->input([ 'message' => $message ]);

        return $this->render($res);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     */
    public function show(Request $req, Response $res, array $args = []): Response
    {
        $this->responseBuffer
             ->input("Hello {$args['name']}.")
             ->input([ 'args' => $args ]);

        return $this->render($res);
    }
}