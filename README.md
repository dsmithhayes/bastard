# Bastard

A really bad RESTful API framework.

## Setup

In Supreme DEVELOPMENT MODE (tm), one should have Vagrant installed and 
available in their system's `PATH`, so they can simply:

    $ vagrant up --provision

And have a fully available Bastard envrionment at `192.168.0.100`, and if you
have the `vagrant-hostsupdater` plugin installed, at `bastard.dev`.

Note that you need `nfs` running on your system to get Bastard going. If you are
on Windows, opt for VBox Shared Folders or Rsync. Its your call.