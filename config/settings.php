<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

/**
 * Slim\App settings
 *
 * @return array
 */
return [
    'displayErrorDetails' => true,
];