<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 *
 * @see https://book.cakephp.org/3.0/en/orm/database-basics.html#configuration
 */

return [
    'db' => [
        'className' => 'Cake\Database\Connection',
        'driver' => 'Cake\Database\Driver\Mysql',
        'persistent' => false,
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'password',
        'database' => 'bastard',
        'encoding' => 'utf8',
        'timezone' => 'UTC',
        'cacheMetadata' => false,
    ]
];