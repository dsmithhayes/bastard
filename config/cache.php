<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

return [
    'cache' => [
        'className' => 'Cake\Cache\Engine\FileEngine',
        'duration' => '+1 Hours',
        'probability' => 100,
        'path' => __DIR__ . '../cache',
    ],
];