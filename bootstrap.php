<?php

/**
 * The bootstrap starts with loading the configurations, and then using it to
 * instantiate a Slim application. From there, the container is filled up with
 * all of its objects. After that the router collects all of the defined routes,
 * and finally the appropriate response is sent down to the user.
 *
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

require_once 'vendor/autoload.php';

/**
 * Collect the application configuration
 */
use Bastard\ConfigReader;
$config = new ConfigReader(__DIR__ . "/config");

/**
 * Instantiate the application.
 */
$app = new Slim\App([ 'settings' => $config->getAll() ]);
$container = $app->getContainer();

/**
 * Hook up the Cache
 */
use Cake\Cache\Cache;
Cache::setConfig('default', $container->settings['cache']);
$container['cache'] = new class {
    public function __get($property)
    {
        return Cache::getConfig((string) $property);
    }
};

/**
 * Global Middleware
 *
 * Note that middleware can be added to individual routes after this point.
 */



/**
 * Collect all of the Routes. Routes should always be collected last in the
 * bootstrapping process. Trust.
 */
use Bastard\Router;
$router = new Router();
$router->register(__DIR__ . "/src/Routes");
$app = $router->run($app);

/**
 * Entry point
 */
return $app;