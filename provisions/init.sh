#!/usr/bin/env bash

##
# Initializtion script for the Vagrant provisions.
#
# Author: Dave Smith-Hayes <dave.smithhayes@mabelslabels.com>
##

##
# Environment variables for use throughout this script
##

VAGRANT_ROOT="/vagrant"
VAGRANT_PROVISIONS="$VAGRANT_ROOT/provisions"
VAGRANT_CONF="$VAGRANT_PROVISIONS/etc"
VAGRANT_SQL="$VAGRANT_PROVISIONS/sql"
VAGRANT_LOGS="$VAGRANT_ROOT/logs"
VAGRANT_NX_LOGS="$VAGRANT_LOGS/nginx"
VAGRANT_PHP_LOGS="$VAGRANT_LOGS/php"
VAGRANT_HOSTNAME="dsh.blog"

MYSQL_DEFAULT_ROOT_PASS="password"

NGINX_CONF="/etc/nginx"
NGINX_CONF_SSL="$NGINX_CONF/ssl"
NGINX_CONF_VHOSTS="$NGINX_CONF/sites-available"
NGINX_CONF_VHOSTS_ENABLED="$NGINX_CONF/sites-enabled"
NGINX_CONF_DEFAULT_VHOST="$NGINX_CONF_VHOSTS/default"
NGINX_CONF_VAGRANT_VHOST="$VAGRANT_CONF/nginx.vhost.conf"

PHP_CONF="/etc/php/7.0/fpm"
PHP_CONF_INI="$PHP_CONF/php.ini"
PHP_CONF_FPM="$PHP_CONF/php-fpm.conf"

SSL_COUNTRY="C"
SSL_STATE="Ontario"
SSL_LOCATION="Hamilton"
SSL_ORGANIZATION="DSH"
SSL_O_UNIT="Dev"
SSL_CN="bastard.dev"

##
# Set up the configuration
##

if [ -f /etc/motd ]; then
    rm /etc/motd
fi

# Backup any existing logs
if [ -d $VAGRANT_LOGS ]; then
    TIMESTAMP=`date +'%s'`
    mv $VAGRANT_LOGS $VAGRANT_LOGS.$TIMESTAMP.bak.d
fi

if ! [ -d $VAGRANT_LOGS ]; then
    mkdir $VAGRANT_LOGS
    mkdir $VAGRANT_NX_LOGS
    mkdir $VAGRANT_PHP_LOGS
fi

cp $VAGRANT_CONF/motd /etc/motd

# For PHP
add-apt-repository -y ppa:ondrej/php

echo "Updating the system..."
apt-get update
apt-get upgrade -y

##
# Base stack installation, pre-configuration save for the MySQL root password
##

echo "Installing the base stack..."

debconf-set-selections <<< "mysql-server-5.6 mysql-server/root_password password $MYSQL_DEFAULT_ROOT_PASS"
debconf-set-selections <<< "mysql-server-5.6 mysql-server/root_password_again password $MYSQL_DEFAULT_ROOT_PASS"

apt-get install -y php7.0 \
                   php7.0-fpm \
                   php7.0-cli \
                   php7.0-sqlite \
                   php7.0-mysql \
                   php7.0-pdo \
                   php7.0-bz2 \
                   php7.0-curl \
                   php7.0-mbstring \
                   php7.0-xml \
                   php7.0-soap \
                   php7.0-opcache \
                   php7.0-gd \
                   php7.0-mcrypt \
                   php7.0-intl \
                   php-redis \
                   php-xdebug \
                   mysql-server-5.6 \
                   mysql-client-5.6 \
                   redis-tools \
                   redis-server \
                   nginx \
                   openssl \
                   htop \
                   vim \
                   sendmail \
                   git

##
# Composer
##

echo "Installing Composer..."
curl -O https://getcomposer.org/composer.phar > composer.phar
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

##
# OpenSSL certificate creation
##

echo "Generating self-signed certificates for NGiNX..."
if [ ! -d $NGINX_CONF_SSL ]; then
    mkdir -p $NGINX_CONF_SSL
fi

# Create the signing key
openssl req -nodes \
            -newkey rsa:2048 \
            -keyout $NGINX_CONF_SSL/server.key \
            -out $NGINX_CONF_SSL/server.csr \
            -subj "/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCATION/O=$SSL_ORGANIZATION/OU=$SSL_O_UNIT/CN=$SSL_CN.blog"

# Create the server's certificate
openssl x509 -req \
             -days 365 \
             -in $NGINX_CONF_SSL/server.csr \
             -signkey $NGINX_CONF_SSL/server.key \
             -out $NGINX_CONF_SSL/server.crt

##
# MySQL Build
##

echo "Creating the database schema..."
mysql -u'root' -p"$MYSQL_DEFAULT_ROOT_PASS" < "$VAGRANT_SQL/init.sql"

for table in "$VAGRANT_SQL/tables"; do
    echo "Creating table '$table'..."
    mysql -u'root' -p"$MYSQL_DEFAULT_ROOT_PASS" < "$table"
done

##
# Clean up
##

echo "Copying the NGiNX and PHP server configuration..."
cp $VAGRANT_CONF/nginx.conf         /etc/nginx/nginx.conf
cp $VAGRANT_CONF/nginx.vhost.conf   /etc/nginx/sites-available/default
cp $VAGRANT_CONF/php.ini            /etc/php/7.0/fpm/php.ini
cp $VAGRANT_CONF/php-fpm-www.conf   /etc/php/7.0/fpm/pool.d/www.conf
cp $VAGRANT_CONF/xdebug.ini         /etc/php/7.0/mods-available/xdebug.ini

if ! [ -d "$VAGRANT_CONF/cache" ]; then
    mkdir "$VAGRANT_SQL/cache"
    chmod 777 $VAGRANT_CONF/cache
fi

# Restart all the services to be safe.
service mysql restart
service php7.0-fpm restart
service nginx restart

# Install the dependencies
su vagrant -c "cd /vagrant; composer install"
