-- author: Dave Smith-Hayes <me@davesmithhayes.com>

use `bastard`;

CREATE TABLE IF NOT EXISTS `users` (
  `id`          INTEGER(128) UNSIGNED   UNIQUE  NOT NULL  AUTO_INCREMENT,
  `username`    VARCHAR(128) UNIQUE             NOT NULL,
  `email`       VARCHAR(128) UNIQUE             NOT NULL,
  `password`    VARCHAR(128),

  INDEX (`email`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB;